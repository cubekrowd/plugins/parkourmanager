# Podman compose test server
Using podman-compose runs a test server which has parkour-manager, WorldEdit and the lobby-games (parkour-manager dependency) running.
Also sets up a mongodb server, so that this doesn't need to be done by the person testing.

To use, cd into this directory and then run the script `./start.sh`.

NOTE: You need to compile the plugin first using `mvn clean package`

## Requirements
 - podman
 - podman-compose
 - Parkour Manager already compiled in the directory above
