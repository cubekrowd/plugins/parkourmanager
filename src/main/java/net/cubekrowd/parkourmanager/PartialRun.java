package net.cubekrowd.parkourmanager;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.DeleteResult;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class PartialRun {

    private String playerUuid;
    private String courseName;
    private int checkpoint;
    private long duration;

    public PartialRun(Player player, Course course, int checkpoint, long duration) {
        playerUuid = player.getUniqueId().toString();
        courseName = course.getName();
        this.checkpoint = checkpoint;
        this.duration = duration;
    }

    public CompletableFuture<Void> save() {
        ParkourManagerPlugin.getInstance().getPartialRuns().put(
                new PlayerAndCourse(playerUuid, courseName),
                this
        );

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("partial_runs", PartialRun.class);
        return ListSubscriber.subscribeWithoutResult(collection.replaceOne(
                Filters.and(Filters.eq("playerUuid", playerUuid), Filters.eq("courseName", courseName)),
                this,
                new ReplaceOptions().upsert(true)
        ));
    }

    public static Map<PlayerAndCourse, PartialRun> loadFromDatabase() {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("partial_runs", PartialRun.class);
        List<PartialRun> partialRuns = ListSubscriber.subscribeAndAwait(collection.find());

        return partialRuns.stream()
                .collect(Collectors.toMap(
                        run -> new PlayerAndCourse(run.playerUuid, run.courseName),
                        run -> run
                ));
    }

    public static PartialRun getOrNull(Player player, Course course) {
        return ParkourManagerPlugin.getInstance().getPartialRuns().get(
                new PlayerAndCourse(player.getUniqueId().toString(), course.getName())
        );
    }

    public static CompletableFuture<Void> clear(Player player, Course course) {
        ParkourManagerPlugin.getInstance().getPartialRuns().remove(
                new PlayerAndCourse(player, course)
        );

        ParkourManagerPlugin.getInstance().getPartialRuns().keySet().removeIf(key ->
                key.getCourseName().equals(course.getName()) &&
                        key.getPlayerUuid().equals(player.getUniqueId().toString()));

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("partial_runs", PartialRun.class);
        return ListSubscriber.subscribeWithoutResult(collection.deleteOne(
                Filters.and(
                        Filters.eq("playerUuid", player.getUniqueId().toString()),
                        Filters.eq("courseName", course.getName())
                )
        ));
    }

    public static CompletableFuture<List<DeleteResult>> clearByCourse(Course course) {
        ParkourManagerPlugin.getLog().info("Clearing partial runs for course " + course.getName());

        ParkourManagerPlugin.getInstance().getPartialRuns().keySet().removeIf(key ->
                key.getCourseName().equals(course.getName()));

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("partial_runs", PartialRun.class);
        return ListSubscriber.subscribe(collection.deleteMany(
                Filters.eq("courseName", course.getName())
        ));
    }

    public static CompletableFuture<List<DeleteResult>> clearAll() {
        ParkourManagerPlugin.getLog().info("Clearing all partial runs");

        ParkourManagerPlugin.getInstance().getPartialRuns().clear();

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("partial_runs", PartialRun.class);
        return ListSubscriber.subscribe(collection.deleteMany(
                // Will be true for every document
                Filters.exists("courseName")
        ));
    }
}
