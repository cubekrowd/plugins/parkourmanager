package net.cubekrowd.parkourmanager;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import simplepets.brainsynder.api.event.entity.PetMountEvent;
import simplepets.brainsynder.api.plugin.SimplePets;

public class SimplePetsWrapper implements Listener {
    public static boolean isEnabled() {
        return Bukkit.getPluginManager().isPluginEnabled("SimplePets");
    }

    public static boolean isPlayerRidingPet(Player player) {
        if (!isEnabled()) {
            return false;
        }
        if (!SimplePets.getUserManager().isUserCached(player)) {
            // @NOTE(traks) If not cached, player joined for the first time
            // since SimplePets got enabled and SimplePets didn't create a
            // user object for them yet.
            //
            // Without this check, we will force SimplePets #75 to create a
            // user object, which will result in the stored 'spawned pets'
            // being spawned from local cache. Then a few ticks later,
            // SimplePets will load the data again from the database in its
            // join listener.
            //
            // This results in the pets spawned a second time, but their
            // UUIDs will be the same, which results in spawning failing and
            // the first spawned pets glitching out (no data, not moving).
            // And SimplePets will think the user has no pets out.
            return false;
        }
        var petUser = SimplePets.getUserManager().getPetUser(player).get();
        var vehicle = player.getVehicle();
        while (vehicle != null) {
            if (SimplePets.isPetEntity(vehicle)) {
                var finalVehicle = vehicle;
                if (petUser.getPetEntities().stream().anyMatch(pe -> pe.getEntity() == finalVehicle)) {
                    return true;
                }
            }
            vehicle = vehicle.getVehicle();
        }
        return false;
    }

    public static void dismountIfRidingPet(Player player) {
        if (isPlayerRidingPet(player)) {
            player.leaveVehicle();
        }
    }

    @EventHandler
    public void onPetMount(PetMountEvent e) {
        var player = e.getEntity().getPetUser().getPlayer();

        Run run = Run.getOrNull(player);
        if (run != null) {
            Run.cancel(player);

            ComponentBuilder message = new ComponentBuilder("Your run was cancelled because you mounted a pet")
                    .color(ChatColor.RED);
            player.spigot().sendMessage(message.create());

            run.sendResumeTipMessage();
        }
    }
}
