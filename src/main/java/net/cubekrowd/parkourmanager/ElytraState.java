package net.cubekrowd.parkourmanager;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.bukkit.inventory.ItemStack;

@NoArgsConstructor
public class ElytraState {
    public boolean shouldHaveElytra = false;
    public ItemStack previousChestplate = null;
    public ItemStack fireworksStack = null;
}
