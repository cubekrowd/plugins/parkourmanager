package net.cubekrowd.parkourmanager;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;

public class PlayerNameLookup {
    public static class UUIDData {
        public UUID uuid;
        public volatile String name;
        public int interest;
        public PlayerProfile profile;
        public volatile boolean inProcessOfFetching;

        public UUIDData(UUID uuid) {
            this.uuid = uuid;
        }
    }

    public Map<UUID, UUIDData> uuidData = new HashMap<>();
    public HttpClient httpClient = HttpClient.newHttpClient();

    public void addInterest(UUID uuid) {
        var data = uuidData.computeIfAbsent(uuid, UUIDData::new);
        data.interest++;

        if (data.profile == null) {
            data.profile = Bukkit.createProfile(uuid);
            if (data.profile.isComplete()) {
                data.name = data.profile.getName();
            }
        }
    }

    public void removeInterest(UUID uuid) {
        var data = uuidData.get(uuid);
        if (data == null) {
            return;
        }
        data.interest--;
        if (data.interest <= 0) {
            uuidData.remove(uuid);
        }
    }

    public String getPlayerName(UUID uuid) {
        var data = uuidData.get(uuid);
        return data == null ? null : data.name;
    }

    public boolean fetchName(UUIDData data) throws IOException, InterruptedException {
        // @NOTE(traks) it seems that Paper's built-in Mojang API query system
        // is broken in 1.16.4 or something, because trying to complete a
        // profile failed constantly on CK. Just implement it ourselves as
        // alternative
        if (data.profile.completeFromCache(true)) {
            // name is in playerdata of world
            data.name = data.profile.getName();
            return true;
        }

        var plugin = ParkourManagerPlugin.getInstance();
        var request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://sessionserver.mojang.com/session/minecraft/profile/" + data.uuid.toString().replace("-", "")))
                .timeout(Duration.ofSeconds(5))
                .build();

        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());

        if (response.statusCode() != 200) {
            plugin.getLogger().warning("Mojang returned " + response.statusCode() + " for name lookup for " + data.uuid);
            return false;
        }

        JsonObject latest;

        try (var stream = response.body()) {
            var buffer = new ByteArrayOutputStream();
            byte[] bufferData = new byte[1024];
            int chunk;
            while ((chunk = stream.read(bufferData, 0, bufferData.length)) != -1) {
                buffer.write(bufferData, 0, chunk);
            }

            byte[] byteArray = buffer.toByteArray();
            latest = new JsonParser().parse(new String(byteArray, StandardCharsets.UTF_8)).getAsJsonObject();
        }

        var name = latest.get("name").getAsString();
        data.name = name;
        return true;
    }

    public void fetchUnknownNames() {
        var plugin = ParkourManagerPlugin.getInstance();

        for (var entry : uuidData.entrySet()) {
            var uuid = entry.getKey();
            var data = entry.getValue();
            if (data.name != null || data.inProcessOfFetching) {
                continue;
            }

            // fetch the name
            data.inProcessOfFetching = true;

            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                try {
                    if (fetchName(data)) {
                        plugin.getLogger().info("Got name " + data.name + " for " + uuid);
                    }
                } catch (IOException | InterruptedException e) {
                    plugin.getLogger().log(Level.WARNING, "Failed to update name for " + uuid, e);
                } finally {
                    data.inProcessOfFetching = false;
                }
            });
        }
    }
}
