package net.cubekrowd.parkourmanager;

import java.util.HashMap;
import java.util.Map;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.Particle.DustOptions;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.StructureBlock;
import org.bukkit.block.data.type.StructureBlock.Mode;
import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.ClickEvent.Action;

@NoArgsConstructor
@AllArgsConstructor
@Data
// Cuboid region that can be selected.
public class CuboidRegion {
    private static Map<Player, CuboidRegion> regions = new HashMap<>();

    public static final BaseComponent[] noRegionSelectedMessage;

    static {
        ComponentBuilder message = new ComponentBuilder("No region selected. Please first ").color(ChatColor.RED);
            message.append("[").color(ChatColor.GRAY);
            message.append("Select a Region")
                .color(ChatColor.WHITE)
                .event(
                new ClickEvent(
                    Action.SUGGEST_COMMAND,
                    "/parkour region wand"
                )
            );
            message.append("]").color(ChatColor.GRAY);
            message.append(" then re-run this command.").color(ChatColor.RED);
        
            noRegionSelectedMessage = message.create();
    }

    public static CuboidRegion getRegion(Player p) {
        if (regions.containsKey(p)) {
            return regions.get(p);
        }

        return null;
    }

    public static void setRegion(Player p, CuboidRegion r) {
        regions.put(p, r);
    }

    @BsonProperty
    private int x1, y1, z1, x2, y2, z2;

    public boolean containsLocation(Location loc) {
        int minX = Math.min(x1, x2);
        int minY = Math.min(y1, y2);
        int minZ = Math.min(z1, z2);
        int maxX = Math.max(x1, x2);
        int maxY = Math.max(y1, y2);
        int maxZ = Math.max(z1, z2);

        return
            minX <= loc.x() && loc.x() <= maxX
            && minY <= loc.y() && loc.y() <= maxY
            && minZ <= loc.z() && loc.z() <= maxZ;
    }

    public void drawSelection(Player p) {
        // @NOTE(cag) The best solution is to draw the region using a structure block...
        // but doing so involves using NMS which I want to avoid. So, for now, I am
        // drawing the bounding box using particles.

        int minX = Math.min(x1, x2);
        int minY = Math.min(y1, y2);
        int minZ = Math.min(z1, z2);
        // add 1, because we want want to draw all corners
        int maxX = Math.max(x1, x2) + 1;
        int maxY = Math.max(y1, y2) + 1;
        int maxZ = Math.max(z1, z2) + 1; 

        DustOptions dustOptions = new DustOptions(
                Color.fromRGB(255, 255, 255), // Colour
                2.0f // Size
        );

        // method:
        // choose TWO of x, y, z axes
        // choose ONE of min, max from each axes
        // fix the values for these axes
        // vary the final axis to create a line
        // => nCr(3, 2) * 2 * 2 = 12 total lines to draw, checks out

        int[][] axes = {
                { minX, maxX },
                { minY, maxY },
                { minZ, maxZ },
        };

        for (int i = 0; i < 3; ++i) {
            // 0 1 2
            // i | x y z
            // ---------
            // 0 | A B .
            // 1 | A . B
            // 2 | . A B
            //
            // A = axis1
            // B = axis2
            // . = varyAxis

            int axis1 = (i == 2) ? 1 : 0;
            int axis2 = (i == 0) ? 1 : 2;
            int varyAxis = 2 - i;

            int[] ax1 = axes[axis1];
            int[] ax2 = axes[axis2];
            int[] varyAx = axes[varyAxis];

            // select one of min, max from each axis
            for (int j = 0; j < 4; ++j) {
                int first = ax1[j / 2];
                int second = ax2[j % 2];

                // vary along remaining axis
                int[] coord = new int[3];

                coord[axis1] = first;
                coord[axis2] = second;

                for (int k = varyAx[0]; k <= varyAx[1]; ++k) {
                    coord[varyAxis] = k;

                    // send particle
                    p.spawnParticle(
                            Particle.DUST,
                            coord[0], // x
                            coord[1], // y
                            coord[2], // z
                            1, // count
                            0, 0, 0, // offset
                            1, // brightness
                            dustOptions // options
                    );
                }
            }

        }
    }
}
