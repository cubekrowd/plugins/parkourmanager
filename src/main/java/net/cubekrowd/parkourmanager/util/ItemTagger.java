package net.cubekrowd.parkourmanager.util;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import net.cubekrowd.parkourmanager.ParkourManagerPlugin;

public class ItemTagger {
    public static void tagItemsStack(ItemStack item, String tag) {
        NamespacedKey key = new NamespacedKey(ParkourManagerPlugin.getInstance(), tag);
        
        ItemMeta data = item.getItemMeta();
        
        data.getPersistentDataContainer().set(key, PersistentDataType.BOOLEAN, true);

        item.setItemMeta(data);
    }

    public static boolean isItemStackTaggedAs(ItemStack item, String tag) {
        NamespacedKey key = new NamespacedKey(ParkourManagerPlugin.getInstance(), tag);

        return item != null && item.getItemMeta().getPersistentDataContainer().has(key, PersistentDataType.BOOLEAN);
    }
}
