package net.cubekrowd.parkourmanager.checkpoints;

import java.util.Locale;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;

@Data
@AllArgsConstructor
@NoArgsConstructor
@BsonDiscriminator(key = "checkpoint-type", value = "cuboid")
public class CuboidCheckpoint extends Checkpoint {

    private CuboidRegion region;

    private double xTp, yTp, zTp;

    float yaw;

    @Override
    public boolean containsLocation(Location loc) {
        return region.containsLocation(loc);
    }

    @Override
    public String toDisplayString() {
        return String.format(Locale.ENGLISH,
                "(Cuboid) (%s, %s, %s), (%s, %s, %s) [%+.3f, %+.3f, %+.3f *%+.3f]", region.getX1(),
                region.getY1(), region.getZ1(), region.getX2(), region.getY2(), region.getZ2(),
                xTp, yTp, zTp, yaw);
    }

    @Override
    public Location getTeleportLocation(Course course) {
        var world = Bukkit.getWorld(course.getWorldName());
        if (world == null) {
            return null;
        }

        return new Location(world, xTp, yTp, zTp, yaw, 0);
    }

}
