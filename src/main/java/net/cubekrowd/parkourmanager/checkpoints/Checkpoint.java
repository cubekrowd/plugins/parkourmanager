package net.cubekrowd.parkourmanager.checkpoints;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import net.cubekrowd.parkourmanager.checkpointeffects.CheckpointEffect;
import net.cubekrowd.parkourmanager.Course;
import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// Abstract class that defines a general type of checkpoint.
//
// Every checkpoint should have the following behaviour:
// - A method to detect if the player is in the checkpoint.
// - Apply effects onto the player, such as giving elytra, potion effects, velocity packets, etc.
// - A location to teleport to.
@Data
@BsonDiscriminator(key = "checkpoint-type")
public abstract class Checkpoint {

    @BsonIgnore
    private Set<Player> players = new HashSet<>();

    public boolean containsPlayer(Player p) {
        return players.contains(p);
    }

    public void addPlayer(Player p) {
        players.add(p);
    }

    public void removePlayer(Player p) {
        if (containsPlayer(p)) {
            players.remove(p);
        }
    }

    @BsonProperty("checkpoint-type")
    protected String checkpointType;

    public static void teleport(Entity entity, Course course, Checkpoint checkpoint) {
        Location location = checkpoint.getTeleportLocation(course);

        if (location == null)
            return;

        entity.teleport(location);
        entity.setVelocity(new Vector());
    }

    // Whether a certain block is inside the checkpoint.
    // NOTE. Assumes the player is in the same world as the block.
    public abstract boolean containsLocation(Location loc);

    // The effects the checkpoint gives the player.
    @EqualsAndHashCode.Exclude
    @BsonProperty
    private List<CheckpointEffect> effects;

    // Whether this checkpoint is restorable.
    @BsonProperty
    @Getter
    @Setter
    private boolean restorable = true;

    // Formats the checkpoint.
    public abstract String toDisplayString();

    // Gets the location to teleport to when going to the checkpoint.
    // Note that this does not necessarily mean all checkpoints are restorable.
    // This is for teleporting to a checkpoint for inspection purposes by staff.
    public abstract Location getTeleportLocation(Course course);

    protected Checkpoint() {
        effects = new ArrayList<>();
    }

    public void applyEffects(Player p) {
        for (CheckpointEffect e : effects) {
            e.apply(p);
        }
    }

    public void addEffect(CheckpointEffect e) {
        effects.add(e);
    }
}
