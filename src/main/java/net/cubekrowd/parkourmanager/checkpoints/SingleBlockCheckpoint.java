package net.cubekrowd.parkourmanager.checkpoints;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.cubekrowd.parkourmanager.Course;
import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.Locale;

@Data
@AllArgsConstructor
@NoArgsConstructor
@BsonDiscriminator(key = "checkpoint-type", value = "single-block")
public class SingleBlockCheckpoint extends Checkpoint {

    @Getter
    private int x;
    @Getter
    private int y;
    @Getter
    private int z;

    @EqualsAndHashCode.Exclude
    private double xOffset;
    @EqualsAndHashCode.Exclude
    private double yOffset;
    @EqualsAndHashCode.Exclude
    private double zOffset;

    @EqualsAndHashCode.Exclude
    private int yaw;

    @Override
    public String toDisplayString() {
        return String.format(Locale.ENGLISH, "(Single Block) %s, %s, %s, [%+.3f, %+.3f, %+.3f *%s]", x, y, z, xOffset, yOffset, zOffset, yaw);
    }

    @Override
    public Location getTeleportLocation(Course course) {
        var world = Bukkit.getWorld(course.getWorldName());
        if (world == null) {
            return null;
        }

        Location location = new Location(
                world,
                x + xOffset,
                y + yOffset,
                z + zOffset,
                yaw, 0
        );

        return location;
    }

    @Override
    public boolean containsLocation(Location loc) {
        return loc.getX() == x && loc.getY() == y && loc.getZ() == z;
    }


}
