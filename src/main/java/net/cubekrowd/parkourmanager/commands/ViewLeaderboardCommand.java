package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.*;
import io.github.llewvallis.commandbuilder.arguments.StringSetArgument;
import net.cubekrowd.parkourmanager.*;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.logging.Level;

@AutoSubCommand(ParkourCommandGroup.class)
public class ViewLeaderboardCommand extends SubCommand {

    @Override
    public String getName() {
        return "top";
    }

    @ExecuteCommand
    private void execute(
            CommandContext ctx,
            Course course,
            @StringSetArgument.Arg({"weekly"}) @OptionalArg String weeklyString
    ) {
        boolean weekly = weeklyString != null;

        Leaderboard.fetchRecords(course, weekly).whenComplete((records, error) -> {
            if (error == null) {
                int runCount = records.size();
                ComponentBuilder builder;

                if (runCount > 0) {
                    builder = new ComponentBuilder("Top " + runCount + " runs:").color(ChatColor.AQUA);

                    for (int i = 0; i < runCount; i++) {
                        CompletedRun record = records.get(i);

                        builder.append("\n" + (i + 1) + ". ").color(ChatColor.GRAY);
                        builder.append(Leaderboard.getPlayerName(record.getPlayerUuid())).color(ChatColor.AQUA);
                        builder.append(" - ").color(ChatColor.GRAY);
                        builder.append(Run.getLongDurationString(record.getDuration())).color(ChatColor.AQUA);
                    }
                } else {
                    builder = new ComponentBuilder("No runs yet").color(ChatColor.RED);
                }

                ctx.getSender().spigot().sendMessage(builder.create());
            } else {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to get records for " + course);
                ComponentBuilder message = new ComponentBuilder("There was an error looking up the leaderboard, please " +
                        "contact a developer if the problem persists").color(ChatColor.RED);
                ctx.getSender().spigot().sendMessage(message.create());
            }
        });
    }
}
