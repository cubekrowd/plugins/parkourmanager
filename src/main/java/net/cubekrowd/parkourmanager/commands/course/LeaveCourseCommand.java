package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Run;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

@AutoSubCommand(ParkourCommandGroup.class)
public class LeaveCourseCommand extends SubCommand {

    @Override
    public String getName() {
        return "leave";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx) {
        boolean cancelled = Run.cancel((Player) ctx.getSender());

        if (cancelled) {
            ComponentBuilder message = new ComponentBuilder("You left the course").color(ChatColor.GREEN);
            ctx.getSender().spigot().sendMessage(message.create());
        } else {
            ComponentBuilder message = new ComponentBuilder("You were not in a course").color(ChatColor.RED);
            ctx.getSender().spigot().sendMessage(message.create());
        }
    }
}
