package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CompositeSubCommand;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;

@AutoSubCommand(ParkourCommandGroup.class)
public class CourseCommandGroup extends CompositeSubCommand {

    @Override
    public String getName() {
        return "course";
    }
}
