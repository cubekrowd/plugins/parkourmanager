package net.cubekrowd.parkourmanager.commands.course;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

@AutoSubCommand(CourseCommandGroup.class)
public class ReloadCoursesCommand extends SubCommand {

    @Override
    public String getName() {
        return "reload";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx) {
        ParkourManagerPlugin.getInstance().reloadData();

        ComponentBuilder message = new ComponentBuilder("Reloaded course data").color(ChatColor.GREEN);
        ctx.getSender().spigot().sendMessage(message.create());
    }
}
