package net.cubekrowd.parkourmanager.commands.region;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.PlayerOnlyCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.NoSuchElementException;

// Testing command to test the region class.
@AutoSubCommand(RegionCommandGroup.class)
public class RegionTestCommand extends SubCommand {

    @Override
    public String getName() {
        return "test";
    }

    @ExecuteCommand
    @PlayerOnlyCommand
    private void execute(CommandContext ctx) {
        Player player = (Player) ctx.getSender();
        
        Location l = player.getLocation();

        int x1 = (int) l.x() + 5;
        int y1 = (int) l.y() + 5;
        int z1 = (int) l.z() + 5;

        int x2 = x1 + 10;
        int y2 = y1 + 10;
        int z2 = z1 + 10;

        CuboidRegion r = new CuboidRegion(x1, y1, z1, x2, y2, z2);

        r.drawSelection(player);
    }
}
