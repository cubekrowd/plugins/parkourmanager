package net.cubekrowd.parkourmanager.commands.region;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CompositeSubCommand;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;

@AutoSubCommand(ParkourCommandGroup.class)
public class RegionCommandGroup extends CompositeSubCommand {

    @Override
    public String getName() {
        return "region";
    }
}
