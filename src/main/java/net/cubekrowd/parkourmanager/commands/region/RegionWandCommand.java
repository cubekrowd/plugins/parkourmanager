package net.cubekrowd.parkourmanager.commands.region;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.PlayerOnlyCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.util.ItemTagger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

// Testing command to test the region class.
@AutoSubCommand(RegionCommandGroup.class)
public class RegionWandCommand extends SubCommand {

    @Override
    public String getName() {
        return "wand";
    }

    @ExecuteCommand
    @PlayerOnlyCommand
    private void execute(CommandContext ctx) {
        Player player = (Player) ctx.getSender();

        ItemStack wand = new ItemStack(Material.STONE_AXE);
        ItemTagger.tagItemsStack(wand, "parkour-region-wand");

        ItemMeta data = wand.getItemMeta();
        List<String> lore = new ArrayList<>();

        lore.add("Left-click to select pos1");
        lore.add("Reft-click to select pos2");

        data.setLore(lore);

        data.setDisplayName("Selection Wand");

        wand.setItemMeta(data);

        player.getInventory().addItem(wand);

        ComponentBuilder message = new ComponentBuilder(
                "You have been given the selection wand. Left-click to select pos1, and right-click to select pos2.")
                .color(ChatColor.AQUA);

        player.sendMessage(message.create());
    }
}
