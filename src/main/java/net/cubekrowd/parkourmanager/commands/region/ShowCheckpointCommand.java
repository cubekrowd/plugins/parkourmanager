package net.cubekrowd.parkourmanager.commands.region;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.PlayerOnlyCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.checkpoints.CuboidCheckpoint;
import net.cubekrowd.parkourmanager.checkpoints.SingleBlockCheckpoint;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

import org.bukkit.entity.Player;

@AutoSubCommand(RegionCommandGroup.class)
public class ShowCheckpointCommand extends SubCommand {

    @Override
    public String getName() {
        return "showcheckpoint";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position) {
        List<Checkpoint> checkpoints = course.getCheckpoints();
        // Migrate from 1 to 0 indexing
        position -= 1;

        Player p = (Player) ctx.getSender();

        try {
            Checkpoint c = checkpoints.get(position);

            if (c instanceof SingleBlockCheckpoint) {
                // just create a single block region
                SingleBlockCheckpoint c_ = (SingleBlockCheckpoint) c;

                new CuboidRegion(c_.getX(), c_.getY(), c_.getZ(), c_.getX(), c_.getY(), c_.getZ()).drawSelection(p);
            } else if (c instanceof CuboidCheckpoint) {
                CuboidCheckpoint c_ = (CuboidCheckpoint) c;

                c_.getRegion().drawSelection(p);
            }
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("No checkpoint exists at position " + (position + 1))
                    .color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }
    }
}
