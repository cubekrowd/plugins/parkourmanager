package net.cubekrowd.parkourmanager.commands.region;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.PlayerOnlyCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.util.ItemTagger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

// Testing command to test the region class.
@AutoSubCommand(RegionCommandGroup.class)
public class RegionShowCommand extends SubCommand {

    @Override
    public String getName() {
        return "show";
    }

    @ExecuteCommand
    @PlayerOnlyCommand
    private void execute(CommandContext ctx) {
        Player player = (Player) ctx.getSender();

        CuboidRegion r = CuboidRegion.getRegion(player);

        if (r == null) {
            ctx.getSender().spigot().sendMessage(CuboidRegion.noRegionSelectedMessage);
            return;
        }

        r.drawSelection(player);
    }
}
