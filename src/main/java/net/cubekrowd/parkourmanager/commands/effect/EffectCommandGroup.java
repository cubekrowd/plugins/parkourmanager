package net.cubekrowd.parkourmanager.commands.effect;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CompositeSubCommand;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;

@AutoSubCommand(ParkourCommandGroup.class)
public class EffectCommandGroup extends CompositeSubCommand {

    @Override
    public String getName() {
        return "effect";
    }
}
