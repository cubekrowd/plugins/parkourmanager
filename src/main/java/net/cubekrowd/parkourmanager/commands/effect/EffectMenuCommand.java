package net.cubekrowd.parkourmanager.commands.effect;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

@AutoSubCommand(EffectCommandGroup.class)
public class EffectMenuCommand extends SubCommand {
    public String getName() {
        return "menu";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position) {
        List<Checkpoint> checkpoints = course.getCheckpoints();
        position -= 1;

        if (position < 0 || position >= checkpoints.size()) {
            BaseComponent[] message = new ComponentBuilder("Checkpoint position " + (position + 1) + " is out of bounds.").color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        ComponentBuilder message = new ComponentBuilder("Add effect to " + course.getName() + " checkpoint " + (position + 1) + ":").color(ChatColor.AQUA);

        // Elytra give
        message.append("\n ┗ Give Elytra")
            .color(ChatColor.WHITE)
            .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/parkour effect add elytragive " + course.getName() + " " + (position + 1)));
        message.append(" (Parameters: [durability] [num. rockets])")
            .color(ChatColor.GRAY);
        // Elytra remove
        message.append("\n ┗ Remove Elytra")
            .color(ChatColor.WHITE)
            .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/parkour effect add elytraremove " + course.getName() + " " + (position + 1)));
            message.append(" (Parameters: None)")
            .color(ChatColor.GRAY);
        
        ctx.getSender().spigot().sendMessage(message.create());
    }
}
