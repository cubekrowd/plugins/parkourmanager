package net.cubekrowd.parkourmanager.commands.effect.add;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CompositeSubCommand;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;
import net.cubekrowd.parkourmanager.commands.effect.EffectCommandGroup;

@AutoSubCommand(EffectCommandGroup.class)
public class EffectAddCommandGroup extends CompositeSubCommand {

    @Override
    public String getName() {
        return "add";
    }
}
