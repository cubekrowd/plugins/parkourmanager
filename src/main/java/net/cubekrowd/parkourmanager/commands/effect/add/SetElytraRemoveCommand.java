package net.cubekrowd.parkourmanager.commands.effect.add;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.checkpointeffects.ElytraCheckpointEffect;
import net.cubekrowd.parkourmanager.checkpointeffects.ElytraRemoveCheckpointEffect;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

@AutoSubCommand(EffectAddCommandGroup.class)
public class SetElytraRemoveCommand extends SubCommand {
    public String getName() {
        return "elytraremove";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position) {
        List<Checkpoint> checkpoints = course.getCheckpoints();
        position -= 1;

        try {
            checkpoints.get(position).addEffect(
                    new ElytraRemoveCheckpointEffect()
            );
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("Checkpoint position " + (position + 1) + " is out of bounds.").color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        course.save().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Successfully added elytra remove effect").color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to save checkpoint effect to the database").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save course " + course.getName(), error);
            }
        });
    }
}
