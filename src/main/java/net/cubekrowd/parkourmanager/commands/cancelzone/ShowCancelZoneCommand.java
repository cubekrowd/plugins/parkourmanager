package net.cubekrowd.parkourmanager.commands.cancelzone;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.PlayerOnlyCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.CancelRunZone;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.entity.Player;

@AutoSubCommand(CancelZoneCommandGroup.class)
public class ShowCancelZoneCommand extends SubCommand {

    @Override
    public String getName() {
        return "show";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position) {
        List<CancelRunZone> zones = course.getZones();
        // Migrate from 1 to 0 indexing
        position -= 1;

        CancelRunZone z;

        try {
            z = zones.get(position);
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("There is no cancel zone at position " + (position + 1))
                    .color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        CuboidRegion r = z.getRegion();
        Player p = (Player) ctx.getSender();

        r.drawSelection(p);
    }
}
