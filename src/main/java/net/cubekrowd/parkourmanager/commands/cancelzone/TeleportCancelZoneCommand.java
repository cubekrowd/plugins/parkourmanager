package net.cubekrowd.parkourmanager.commands.cancelzone;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.PlayerOnlyCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.CancelRunZone;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.entity.Player;

@AutoSubCommand(CancelZoneCommandGroup.class)
public class TeleportCancelZoneCommand extends SubCommand {

    @Override
    public String getName() {
        return "teleport";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position) {
        List<CancelRunZone> zones = course.getZones();
        // Migrate from 1 to 0 indexing
        position -= 1;

        CancelRunZone z;

        try {
            z = zones.get(position);
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("There is no cancel zone at position " + (position + 1))
                    .color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        // get midpoint
        CuboidRegion r = z.getRegion();

        double x1 = r.getX1(), y1 = r.getY1(), z1 = r.getZ1(),
                x2 = r.getX2(), y2 = r.getY2(), z2 = r.getZ2();

        double midX = (x1 + x2 + 1) / 2;
        double midY = (y1 + y2 + 1) / 2;
        double midZ = (z1 + z2 + 1) / 2;

        Player p = (Player) ctx.getSender();

        p.teleport(new Location(p.getWorld(), midX, midY, midZ));
    }
}
