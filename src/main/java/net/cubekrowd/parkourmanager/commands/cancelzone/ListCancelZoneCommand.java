package net.cubekrowd.parkourmanager.commands.cancelzone;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.CancelRunZone;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.logging.Level;

@AutoSubCommand(CancelZoneCommandGroup.class)
public class ListCancelZoneCommand extends SubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course) {
        ctx.getSender().spigot().sendMessage(course.generateZonesMessage());
    }
}
