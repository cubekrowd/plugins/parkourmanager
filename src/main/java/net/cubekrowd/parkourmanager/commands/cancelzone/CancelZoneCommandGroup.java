package net.cubekrowd.parkourmanager.commands.cancelzone;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CompositeSubCommand;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;

@AutoSubCommand(ParkourCommandGroup.class)
public class CancelZoneCommandGroup extends CompositeSubCommand {

    @Override
    public String getName() {
        return "cancelzone";
    }
}
