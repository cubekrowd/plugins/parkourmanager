package net.cubekrowd.parkourmanager.commands.cancelzone;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.CancelRunZone;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.logging.Level;

@AutoSubCommand(CancelZoneCommandGroup.class)
public class CreateCancelZoneCommand extends SubCommand {

    @Override
    public String getName() {
        return "create";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(CommandContext ctx, Course course) {
        Player p = (Player) ctx.getSender();

        CuboidRegion r = CuboidRegion.getRegion(p);

        if (r == null) {
            ctx.getSender().spigot().sendMessage(CuboidRegion.noRegionSelectedMessage);
            return;
        }
        
        course.getZones().add(new CancelRunZone(r));

        course.save().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Added cancel zone").color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to save cancel zone to the database").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save course " + course.getName(), error);
            }
        });
    }
}
