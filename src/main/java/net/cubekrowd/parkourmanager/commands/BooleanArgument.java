package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;

/**
 * An argument that matches optionally bounded integers.
 */
@With
@NoArgsConstructor
public class BooleanArgument implements ArgumentParser<Boolean> {

    @ParserAnnotation(BooleanArgument.class)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface Arg {

        int min() default Integer.MIN_VALUE;
        int max() default Integer.MAX_VALUE;
    }

    @ArgumentInferenceFactory
    private static BooleanArgument createParserFromAnnotation(ArgumentInferenceContext<Arg> ctx) {
        return new BooleanArgument();
    }

    @Override
    public Boolean parse(String argument, int position, CommandContext context) throws ArgumentParseException {
        boolean value;
        try {
            value = Boolean.parseBoolean(argument);
        } catch (NumberFormatException e) {
            throw new ArgumentParseException("not a valid boolean");
        }

        return value;
    }

    @Override
    public Set<String> complete(List<Object> parsedArguments, String currentArgument, int position, CommandContext context) {
        return new HashSet<>(Arrays.asList("true", "false"));
    }
}