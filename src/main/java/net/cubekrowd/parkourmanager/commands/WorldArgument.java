package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.ArgumentParseException;
import io.github.llewvallis.commandbuilder.ArgumentParser;
import io.github.llewvallis.commandbuilder.CommandContext;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class WorldArgument implements ArgumentParser<World> {

    @Override
    public World parse(String argument, int position, CommandContext context) throws ArgumentParseException {
        World world = Bukkit.getWorld(argument);
        if (world == null) {
            throw new ArgumentParseException("world " + argument + " does not exist");
        }

        return world;
    }

    @Override
    public Set<String> complete(List<Object> parsedArguments, String currentArgument, int position, CommandContext context) {
        return Bukkit.getWorlds().stream()
                .map(World::getName)
                .collect(Collectors.toSet());
    }
}
