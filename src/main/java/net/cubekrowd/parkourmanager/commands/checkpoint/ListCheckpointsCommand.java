package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.checkpoints.CuboidCheckpoint;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.checkpointeffects.CheckpointEffect;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;

@AutoSubCommand(CheckpointCommandGroup.class)
public class ListCheckpointsCommand extends SubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx, Course course) {
        ctx.getSender().spigot().sendMessage(course.generateCheckpointsMessage());
    }
}
