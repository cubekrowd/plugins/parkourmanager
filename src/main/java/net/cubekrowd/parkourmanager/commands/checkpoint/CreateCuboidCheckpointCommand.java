package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.*;
import io.github.llewvallis.commandbuilder.arguments.BlockCoordArgument;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.checkpoints.CuboidCheckpoint;
import net.cubekrowd.parkourmanager.checkpoints.SingleBlockCheckpoint;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.commands.ContinuousCoordinateArgument;
import net.cubekrowd.parkourmanager.commands.YawArgument;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.ClickEvent.Action;

import java.util.List;
import java.util.logging.Level;

import org.bukkit.entity.Player;

@AutoSubCommand(CheckpointCommandGroup.class)
public class CreateCuboidCheckpointCommand extends SubCommand {

    @Override
    public String getName() {
        return "createcuboid";
    }

    @PlayerOnlyCommand
    @ExecuteCommand
    private void execute(
            CommandContext ctx,
            Course course,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.X) double xTp,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.Y) double yTp,
            @ContinuousCoordinateArgument.Arg(ContinuousCoordinateArgument.Axis.Z) double zTp,
            @ClassArg(YawArgument.class) int yaw,
            @OptionalArg Boolean restorable,
            @OptionalArg Integer position
    ) {
        if (restorable == null) {
            restorable = true;
        }

        Player p = (Player) ctx.getSender();

        CuboidRegion r = CuboidRegion.getRegion(p);

        if (r == null) {
            ctx.getSender().spigot().sendMessage(CuboidRegion.noRegionSelectedMessage);
            return;
        }

        Checkpoint checkpoint = new CuboidCheckpoint(
                r, xTp, yTp, zTp,
                Math.floorMod(yaw, 360)
        );

        checkpoint.setRestorable(restorable);

        List<Checkpoint> checkpoints = course.getCheckpoints();

        if (checkpoints.contains(checkpoint)) {
            BaseComponent[] message = new ComponentBuilder("A checkpoint already exists in that location").color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        if (position == null) {
            position = checkpoints.size();
        } else {
            // Migrate from 1 to 0 indexing
            position -= 1;
        }

        try {
            checkpoints.add(position, checkpoint);
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("Cannot add a checkpoint at position " + (position + 1)).color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        course.save().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Added checkpoint").color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to save checkpoint to the database").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save course " + course.getName(), error);
            }
        });
    }
}
