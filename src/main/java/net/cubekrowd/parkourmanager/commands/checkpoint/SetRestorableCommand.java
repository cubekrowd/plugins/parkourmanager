package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.arguments.StringSetArgument;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.OptionalArg;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.util.List;
import java.util.logging.Level;

@AutoSubCommand(CheckpointCommandGroup.class)
public class SetRestorableCommand extends SubCommand {

    @Override
    public String getName() {
        return "setrestorable";
    }

    // @NOTE(cag) seems the command library doesn't support boolean argument so I just threw this in as a quick hack 
    @ExecuteCommand
    private void execute(CommandContext ctx, Course course, int position, @OptionalArg Boolean newVal) {
        List<Checkpoint> checkpoints = course.getCheckpoints();
        // Migrate from 1 to 0 indexing
        position -= 1;

        Checkpoint checkpoint;
        
        try {
            checkpoint = checkpoints.get(position);
        } catch (IndexOutOfBoundsException e) {
            BaseComponent[] message = new ComponentBuilder("Cannot set the restorability of a checkpoint at position " + (position + 1)).color(ChatColor.RED).create();
            ctx.getSender().spigot().sendMessage(message);
            return;
        }

        String ogState = checkpoint.isRestorable() ? "True" : "False";

        if (newVal == null) {
            newVal = !checkpoint.isRestorable();
        }

        checkpoint.setRestorable(newVal);
        
        String newState = newVal ? "True" : "False";

        course.save().whenComplete((nothing, error) -> {
            if (error == null) {
                BaseComponent[] message = new ComponentBuilder("Set checkpoint restorability to " + newState + " (was " + ogState + ")").color(ChatColor.GREEN).create();
                ctx.getSender().spigot().sendMessage(message);
            } else {
                BaseComponent[] message = new ComponentBuilder("Failed to set checkpoint restorability").color(ChatColor.RED).create();
                ctx.getSender().spigot().sendMessage(message);
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to save course " + course.getName(), error);
            }
        });
    }
}
