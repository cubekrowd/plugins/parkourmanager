package net.cubekrowd.parkourmanager.commands.checkpoint;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CompositeSubCommand;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;

@AutoSubCommand(ParkourCommandGroup.class)
public class CheckpointCommandGroup extends CompositeSubCommand {

    @Override
    public String getName() {
        return "checkpoint";
    }
}
