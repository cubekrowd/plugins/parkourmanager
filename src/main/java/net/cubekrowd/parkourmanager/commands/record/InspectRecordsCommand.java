package net.cubekrowd.parkourmanager.commands.record;

import io.github.llewvallis.commandbuilder.*;
import io.github.llewvallis.commandbuilder.arguments.StringSetArgument;
import net.cubekrowd.parkourmanager.CompletedRun;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.Run;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;

@AutoSubCommand(RecordCommandGroup.class)
public class InspectRecordsCommand extends SubCommand {

    private static final int PAGE_SIZE = 15;

    @Override
    public String getName() {
        return "inspect";
    }

    @ExecuteCommand
    private void execute(
            CommandContext ctx,
            String playerName,
            @StringSetArgument.Arg({ "recent", "fastest" }) String sortMode,
            int page,
            @OptionalArg Course courseNullable
    ) {
        if (page < 1) {
            ComponentBuilder message = new ComponentBuilder("Pages start at 1").color(ChatColor.RED);
            ctx.getSender().spigot().sendMessage(message.create());
            return;
        }

        boolean sortByRecency = sortMode.equals("recent");

        // UUID lookup can require a network request, so do it async
        CompletableFuture.runAsync(() -> {
            // Deprecated since UUIDs are generally better, but in this case we do want to do it by name for convenience
            @SuppressWarnings("deprecation")
            OfflinePlayer player = Bukkit.getOfflinePlayer(playerName);
            // Note that this does work even if the player does not exist
            UUID playerUuid = player.getUniqueId();

            CompletedRun.fetchRuns(
                    courseNullable,
                    playerUuid,
                    true,
                    sortByRecency,
                    false,
                    page - 1,
                    PAGE_SIZE
            ).whenComplete((runs, error) -> {
                if (error == null) {
                    if (runs.size() == 0) {
                        ComponentBuilder message = new ComponentBuilder("There are no records on this page").color(ChatColor.RED);
                        ctx.getSender().spigot().sendMessage(message.create());
                        return;
                    }

                    ComponentBuilder builder = new ComponentBuilder("Showing page " + page + " (click to show delete command):")
                            .color(ChatColor.BLUE)
                            .underlined(true);

                    for (int i = 0; i < runs.size(); i++) {
                        CompletedRun run = runs.get(i);

                        int runNumber = i + (page - 1) * PAGE_SIZE + 1;
                        builder.append("\n" + runNumber + ". ")
                                .color(ChatColor.GRAY)
                                .underlined(false)
                                .event(new ClickEvent(
                                        ClickEvent.Action.SUGGEST_COMMAND,
                                        "/parkour record delete " + run.getRunId().toHexString()
                                ));

                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yy HH:mm:ss", Locale.ENGLISH);
                        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        String dateString = dateFormat.format(run.getWhen());

                        builder.append(run.getCourseName() + " ").color(ChatColor.AQUA);
                        builder.append(Run.getLongDurationString(run.getDuration()) + " ").color(ChatColor.GREEN);
                        builder.append(dateString).color(ChatColor.GRAY);
                    }

                    builder.append("\n");

                    String courseArgumentString = "";
                    if (courseNullable != null) {
                        courseArgumentString = " " + courseNullable.getName();
                    }

                    if (page > 1) {
                        builder.append("Previous page").color(ChatColor.GREEN).bold(true)
                                .event(new ClickEvent(
                                        ClickEvent.Action.RUN_COMMAND,
                                        "/parkour record inspect " + playerName + " " + sortMode + " " + (page - 1) + courseArgumentString
                                ));

                        builder.append(" | ").color(ChatColor.GRAY).bold(false).event((ClickEvent) null);
                    }

                    builder.append("Next page").color(ChatColor.GREEN).bold(true)
                            .event(new ClickEvent(
                                    ClickEvent.Action.RUN_COMMAND,
                                    "/parkour record inspect " + playerName + " " + sortMode + " " + (page + 1) + courseArgumentString
                            ));

                    ctx.getSender().spigot().sendMessage(builder.create());
                } else {
                    ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to fetch records for " + playerName, error);
                    ComponentBuilder message = new ComponentBuilder("Failed to get records for " + playerName).color(ChatColor.RED);
                    ctx.getSender().spigot().sendMessage(message.create());
                }
            });
        });
    }
}
