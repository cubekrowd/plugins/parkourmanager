package net.cubekrowd.parkourmanager.commands.record;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CompositeSubCommand;
import net.cubekrowd.parkourmanager.commands.ParkourCommandGroup;

@AutoSubCommand(ParkourCommandGroup.class)
public class RecordCommandGroup extends CompositeSubCommand {

    @Override
    public String getName() {
        return "record";
    }
}
