package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.AutoCommand;
import io.github.llewvallis.commandbuilder.CompositeCommandBuilder;
import io.github.llewvallis.commandbuilder.CompositeTopLevelCommand;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

@AutoCommand
public class ParkourCommandGroup extends CompositeTopLevelCommand {

    @Override
    public String getName() {
        return "parkour";
    }

    @Override
    protected void configure(CompositeCommandBuilder builder) {
        builder
                .helpMessageTheme(
                        new CompositeCommandBuilder.HelpMessageTheme()
                                .withBorderColor(ChatColor.GRAY)
                                .withHeadingColor(ChatColor.GREEN)
                                .withLabelColor(ChatColor.AQUA)
                                .withTextColor(ChatColor.GRAY)
                )
                .onEmptyInvocation(sender -> {
                    ComponentBuilder message = new ComponentBuilder("Courses (click to teleport)")
                            .color(ChatColor.AQUA);

                    for (Course course : ParkourManagerPlugin.getInstance().getCourses()) {
                        message.append("\n- ")
                                .color(ChatColor.GRAY)
                                .event(new ClickEvent(
                                        ClickEvent.Action.RUN_COMMAND,
                                        "/parkour play " + course.getName()
                                ));

                        message.append(course.getName()).color(ChatColor.AQUA);
                    }

                    message
                            .append("\nDo '/parkour help' to see available commands")
                            .event((ClickEvent) null)
                            .color(ChatColor.AQUA);

                    sender.spigot().sendMessage(message.create());
                });
    }
}
