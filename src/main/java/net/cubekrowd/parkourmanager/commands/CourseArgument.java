package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.ArgumentParseException;
import io.github.llewvallis.commandbuilder.ArgumentParser;
import io.github.llewvallis.commandbuilder.CommandContext;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CourseArgument implements ArgumentParser<Course> {

    @Override
    public Course parse(String argument, int position, CommandContext context) throws ArgumentParseException {
        if (Course.exists(argument)) {
            return Course.byName(argument);
        } else {
            throw new ArgumentParseException("course " + argument + " does not exist");
        }
    }

    @Override
    public Set<String> complete(List<Object> parsedArguments, String currentArgument, int position, CommandContext context) {
        return ParkourManagerPlugin.getInstance().getCourses().stream()
                .map(Course::getName)
                .collect(Collectors.toSet());
    }
}
