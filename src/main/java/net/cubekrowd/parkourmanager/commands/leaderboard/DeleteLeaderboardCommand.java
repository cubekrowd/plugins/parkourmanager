package net.cubekrowd.parkourmanager.commands.leaderboard;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Leaderboard;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.Optional;
import java.util.logging.Level;

@AutoSubCommand(LeaderboardCommandGroup.class)
public class DeleteLeaderboardCommand extends SubCommand {

    @Override
    public String getName() {
        return "delete-near";
    }

    @ExecuteCommand
    @PlayerOnlyCommand
    private void execute(CommandContext ctx) {
        Leaderboard.warnIfInvisible(ctx.getSender());
        Player player = (Player) ctx.getSender();

        Optional<Leaderboard> leaderboardOptional = ParkourManagerPlugin.getInstance().getLeaderboards().stream()
                .filter(leaderboard -> leaderboard.getWorldName().equals(player.getWorld().getName()))
                .filter(leaderboard -> distance(leaderboard, player) <= 5)
                .min(Comparator.comparingDouble(leaderboard -> distance(leaderboard, player)));

        leaderboardOptional.ifPresentOrElse(leaderboard -> {
            leaderboard.remove().whenComplete((nothing, error) -> {
                if (error == null) {
                    ComponentBuilder message = new ComponentBuilder("Deleted a leaderboard for course " + leaderboard.getCourseName())
                            .color(ChatColor.GREEN);
                    player.spigot().sendMessage(message.create());
                } else {
                    ComponentBuilder message = new ComponentBuilder("Failed to delete leaderboard for course " +
                            leaderboard.getCourseName() + " from the database").color(ChatColor.RED);
                    player.spigot().sendMessage(message.create());

                    ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to delete leaderboard for " +
                            leaderboard.getCourseName(), error);
                }
            });
        }, () -> {
            ComponentBuilder message = new ComponentBuilder("No nearby leaderboards").color(ChatColor.RED);
            player.spigot().sendMessage(message.create());
        });
    }

    private double distance(Leaderboard leaderboard, Player player) {
        Location playerLocation = player.getLocation();
        return Math.sqrt(
                Math.pow(leaderboard.getX() - playerLocation.getX(), 2) +
                Math.pow(leaderboard.getY() - playerLocation.getY(), 2) +
                Math.pow(leaderboard.getZ() - playerLocation.getZ(), 2)
        );
    }
}
