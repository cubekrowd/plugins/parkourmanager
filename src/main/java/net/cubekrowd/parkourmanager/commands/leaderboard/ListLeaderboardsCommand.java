package net.cubekrowd.parkourmanager.commands.leaderboard;

import io.github.llewvallis.commandbuilder.AutoSubCommand;
import io.github.llewvallis.commandbuilder.CommandContext;
import io.github.llewvallis.commandbuilder.ExecuteCommand;
import io.github.llewvallis.commandbuilder.SubCommand;
import net.cubekrowd.parkourmanager.Leaderboard;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

@AutoSubCommand(LeaderboardCommandGroup.class)
public class ListLeaderboardsCommand extends SubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @ExecuteCommand
    private void execute(CommandContext ctx) {
        Leaderboard.warnIfInvisible(ctx.getSender());

        ComponentBuilder message = new ComponentBuilder("Leaderboards for " + ParkourManagerPlugin.getServerId() + ":")
                .color(ChatColor.AQUA);

        ParkourManagerPlugin.getInstance().getLeaderboards().forEach(leaderboard -> {
            message.append("\n- ").color(ChatColor.GRAY);
            message.append(leaderboard.getCourseName()).color(ChatColor.AQUA);
            message.append(" - ").color(ChatColor.GRAY);
            message.append(String.format(
                    "%.3f %.3f %.3f (%s)",
                    leaderboard.getX(), leaderboard.getY(), leaderboard.getZ(),
                    leaderboard.getWorldName()
            ) )
                    .color(ChatColor.AQUA);
        });

        ctx.getSender().spigot().sendMessage(message.create());
    }
}
