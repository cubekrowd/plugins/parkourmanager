package net.cubekrowd.parkourmanager.commands;

import io.github.llewvallis.commandbuilder.*;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.PartialRun;
import net.cubekrowd.parkourmanager.Run;
import net.cubekrowd.parkourmanager.SimplePetsWrapper;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

@AutoSubCommand(ParkourCommandGroup.class)
public class ResumeRunCommand extends SubCommand {

    @Override
    public String getName() {
        return "resume";
    }

    @ExecuteCommand
    @PlayerOnlyCommand
    private void execute(CommandContext ctx, Course course) {
        Player player = (Player) ctx.getSender();

        PartialRun partialRun = PartialRun.getOrNull(player, course);
        if (partialRun == null) {
            ComponentBuilder message = new ComponentBuilder("You have no run to resume for " + course.getName())
                    .color(ChatColor.RED);
            player.spigot().sendMessage(message.create());
            return;
        }

        Run oldRun = Run.getOrNull(player);
        if (oldRun != null && oldRun.hasReachedCheckpoint() && oldRun.getCourse().equals(course)) {
            ComponentBuilder message = new ComponentBuilder("You are already in that course")
                    .color(ChatColor.RED);
            player.spigot().sendMessage(message.create());
            return;
        }

        Run.cancel(player);

        player.setFlying(false);
        player.setWalkSpeed(0.2f);
        Run newRun = Run.resume(player, course, partialRun.getCheckpoint(), partialRun.getDuration());

        SimplePetsWrapper.dismountIfRidingPet(player);

        newRun.teleportToLastCheckpoint();
    }
}
