package net.cubekrowd.parkourmanager;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// In the future, can be extended into an abstract class.
@Data
@NoArgsConstructor
public class CancelRunZone {

    // @NOTE(cag) reused code from checkpoints, might be good to refactor at some point
    @BsonIgnore
    private Set<Player> players = new HashSet<>();

    public boolean containsPlayer(Player p) {
        return players.contains(p);
    }

    public void addPlayer(Player p) {
        players.add(p);
    }

    public void removePlayer(Player p) {
        if (containsPlayer(p)) {
            players.remove(p);
        }
    }

    @BsonProperty
    private CuboidRegion region;

    // for future migration purposes
    @BsonProperty("zone-type")
    private String zoneType = "cancel-run";

    public CancelRunZone(CuboidRegion r) {
        region = r;
    }

    public String toDisplayString() {
        return String.format(Locale.ENGLISH,
                "(%s, %s, %s), (%s, %s, %s)", region.getX1(),
                region.getY1(), region.getZ1(), region.getX2(), region.getY2(), region.getZ2());
    }
}
