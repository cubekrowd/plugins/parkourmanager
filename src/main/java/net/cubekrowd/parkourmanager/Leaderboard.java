package net.cubekrowd.parkourmanager;

import com.mongodb.client.model.Filters;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Leaderboard {

    @BsonId
    @EqualsAndHashCode.Include
    private ObjectId leaderboardId = ObjectId.get();

    private String courseName;

    private String worldName;
    private String serverId = ParkourManagerPlugin.getServerId();

    private double x;
    private double y;
    private double z;

    private boolean weekly;

    @BsonIgnore
    private List<CompletedRun> lastRecords = null;
    @BsonIgnore
    private WeakReference<World> lastWorld = new WeakReference<>(null);

    @BsonIgnore
    public boolean allPlayerNamesKnown;
    @BsonIgnore
    public boolean removed;

    public Leaderboard(Course course, Location location, boolean weekly) {
        courseName = course.getName();
        worldName = location.getWorld().getName();
        x = location.getX();
        y = location.getY();
        z = location.getZ();
        this.weekly = weekly;
    }

    public void render() {
        Course course = ParkourManagerPlugin.getInstance().getCourses().stream()
                .filter(c -> c.getName().equals(courseName))
                .findAny().orElse(null);

        if (course == null) {
            ParkourManagerPlugin.getLog().warning("Leaderboard for course " + courseName + " cannot be updated " +
                    "because that course does not exist");
            return;
        }

        fetchRecords(course, weekly).whenComplete((records, error) -> {
            if (error == null) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(ParkourManagerPlugin.getInstance(), () -> renderNow(records));
            } else {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to fetch leaderboard lines during update", error);
            }
        });
    }

    public void updateNameLookupInterest(List<CompletedRun> newRecords) {
        var lookup = ParkourManagerPlugin.getInstance().playerNameLookup;

        allPlayerNamesKnown = true;

        if (newRecords != null) {
            newRecords.forEach(r -> {
                var uuid = UUID.fromString(r.getPlayerUuid());
                lookup.addInterest(uuid);
                if (lookup.getPlayerName(uuid) == null) {
                    allPlayerNamesKnown = false;
                }
            });
        }

        if (lastRecords != null) {
            lastRecords.forEach(r -> lookup.removeInterest(UUID.fromString(r.getPlayerUuid())));
        }
    }

    private void renderNow(List<CompletedRun> records) {
        if (removed) {
            // Don't create holograms and stuff after the leaderboard has
            // been removed
            return;
        }

        var world = Bukkit.getWorld(worldName);
        var prevWorld = lastWorld.get();
        // @TODO(traks) currently leaderboards don't update if you log out,
        // change name, and log back in
        if (allPlayerNamesKnown && records.equals(lastRecords) && world == prevWorld) {
            return;
        }

        updateNameLookupInterest(records);

        lastRecords = records;
        lastWorld = new WeakReference<>(world);

        List<String> lines = new ArrayList<>();

        if (weekly) {
            lines.add(ChatColor.GREEN.toString() + ChatColor.BOLD + "Weekly records");
        } else {
            lines.add(ChatColor.GREEN.toString() + ChatColor.BOLD + "All time records");
        }

        for (CompletedRun record : records) {
            lines.add(String.format(
                    "%s - %s",
                    ChatColor.AQUA + getPlayerName(record.getPlayerUuid()) + ChatColor.GRAY,
                    ChatColor.AQUA + Run.getLongDurationString(record.getDuration())
            ));
        }

        if (records.size() == 0) {
            lines.add(ChatColor.RED + "None");
        }

        if (world != prevWorld) {
            HologramsWrapper.removeHolo(leaderboardId);
        }

        if (world != null) {
            HologramsWrapper.renderHolo(
                    leaderboardId,
                    new Location(world, x, y + 4, z),
                    lines
            );
        }

        // @TODO(traks) podiums don't handle unloaded worlds properly
        if (records.size() == 0) {
            CitizensWrapper.removePodiumIfExists(leaderboardId);
        } else {
            // @TODO(traks) what if the player name couldn't be determined?
            // Which fake player will be displayed?
            String playerName = getPlayerName(records.get(0).getPlayerUuid());

            CitizensWrapper.renderPodium(
                    leaderboardId,
                    new Location(world, x, y, z),
                    playerName
            );
        }
    }

    public CompletableFuture<Void> save() {
        // Remove before adding to propogate any updates
        ParkourManagerPlugin.getInstance().getLeaderboards().remove(this);
        ParkourManagerPlugin.getInstance().getLeaderboards().add(this);

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("leaderboards", Leaderboard.class);
        return ListSubscriber.subscribeWithoutResult(collection.insertOne(this));
    }

    public CompletableFuture<Void> remove() {
        removed = true;
        HologramsWrapper.removeHolo(leaderboardId);
        CitizensWrapper.removePodiumIfExists(leaderboardId);
        updateNameLookupInterest(null);

        ParkourManagerPlugin.getInstance().getLeaderboards().remove(this);

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("leaderboards", Leaderboard.class);
        return ListSubscriber.subscribeWithoutResult(collection.deleteOne(Filters.eq("_id", leaderboardId)));
    }

    public static CompletableFuture<Leaderboard> create(Course course, Location location, boolean weekly) {
        Leaderboard leaderboard = new Leaderboard(course, location, weekly);
        leaderboard.render();

        return leaderboard.save().thenApply(nothing -> leaderboard);
    }

    public static Set<Leaderboard> loadFromDatabase() {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("leaderboards", Leaderboard.class);

        List<Leaderboard> leaderboards = ListSubscriber.subscribeAndAwait(collection.find().filter(
                Filters.eq("serverId", ParkourManagerPlugin.getServerId())
        ));

        return new HashSet<>(leaderboards);
    }

    public static CompletableFuture<List<CompletedRun>> fetchRecords(Course course, boolean weekly) {
        return CompletedRun.fetchRuns(course, null, false, false, weekly, 0, 5);
    }

    public static void warnIfInvisible(CommandSender sender) {
        if (HologramsWrapper.getInstance() == null && CitizensWrapper.getInstance() == null) {
            ComponentBuilder message = new ComponentBuilder("Neither HolographicDisplays nor Citizens are installed," +
                    " leaderboards will not be visible").color(ChatColor.RED);
            sender.spigot().sendMessage(message.create());
        }
    }

    public static String getPlayerName(String uuidString) {
        UUID uuid = UUID.fromString(uuidString);

        var lookup = ParkourManagerPlugin.getInstance().playerNameLookup;
        var name = lookup.getPlayerName(uuid);
        if (name == null) {
            name = uuid.toString();
        }
        return name;
    }
}
