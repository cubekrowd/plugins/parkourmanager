package net.cubekrowd.parkourmanager;

import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import eu.decentsoftware.holograms.api.DHAPI;
import eu.decentsoftware.holograms.api.holograms.Hologram;

import java.util.List;

public class HologramsWrapper {

    private static HologramsWrapper instance = null;

    public static HologramsWrapper getInstance() {
        if (instance != null) {
            return instance;
        }

        if (Bukkit.getPluginManager().isPluginEnabled("DecentHolograms")) {
            instance = new HologramsWrapper();
            return instance;
        }

        return null;
    }

    public static void renderHolo(ObjectId key, Location location, List<String> lines) {
        if (getInstance() != null) {
            getInstance().render(key, location, lines);
        }
    }

    private void render(ObjectId key, Location location, List<String> lines) {

        String name = key.toHexString();

        Hologram hologram = DHAPI.getHologram(name);
        if (hologram == null) {
            hologram = DHAPI.createHologram(name, location);
        }

        DHAPI.setHologramLines(hologram, lines);
    }

    public static void removeHolo(ObjectId key) {
        if (getInstance() != null) {
            getInstance().remove(key);
        }
    }

    private void remove(ObjectId key) {
        Hologram hologram = DHAPI.getHologram(key.toString());
        if (hologram == null) {
            ParkourManagerPlugin.getInstance().getLogger().warning("Attempting to delete untracked holograph");
            return;
        }

        hologram.delete();
    }
}
