package net.cubekrowd.parkourmanager.listeners;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import net.cubekrowd.parkourmanager.util.ItemTagger;

public class UnlimitedFireworksListener implements Listener {
    @EventHandler
    private void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        // if player is in creative mode, ignore
        if (p.getGameMode() == GameMode.CREATIVE)
            return;

        ItemStack item = e.getItem();

        if (!ItemTagger.isItemStackTaggedAs(item, "parkour-fireworks-unlimited"))
            return;

        // for whatever reason, when flying using elytra, if you use a firework,
        // it will send an extra LEFT_CLICK_AIR event... so we just ignore this
        if (e.getAction() == Action.LEFT_CLICK_AIR) return;

        if (item.getAmount() > 1)
            return;
         
        item.setAmount(2);
    }
}
