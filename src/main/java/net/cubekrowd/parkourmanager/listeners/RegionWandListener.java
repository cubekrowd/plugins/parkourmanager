package net.cubekrowd.parkourmanager.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import net.cubekrowd.parkourmanager.CuboidRegion;
import net.cubekrowd.parkourmanager.util.ItemTagger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class RegionWandListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getClickedBlock() == null)
            return;

        Player p = e.getPlayer();

        ItemStack item = e.getItem();

        if (!ItemTagger.isItemStackTaggedAs(item, "parkour-region-wand"))
            return;

        Location l = e.getClickedBlock().getLocation();

        int x = (int) l.x(), y = (int) l.y(), z = (int) l.z();

        boolean pos = e.getAction().isLeftClick();

        // Send message
        if (pos) {
            BaseComponent[] msg = new ComponentBuilder(
                    "Position 1 set to (" + l.x() + ", " + l.y() + ", " + l.z() + ").")
                    .color(ChatColor.GREEN).create();
            p.sendMessage(msg);
        } else {
            BaseComponent[] msg = new ComponentBuilder(
                    "Position 2 set to (" + l.x() + ", " + l.y() + ", " + l.z() + ").")
                    .color(ChatColor.GREEN).create();
            p.sendMessage(msg);
        }

        // If region not set, then just make a new region
        CuboidRegion r = CuboidRegion.getRegion(p);

        CuboidRegion newRegion;

        if (r == null) {
            newRegion = new CuboidRegion(x, y, z, x, y, z);
        } else {
            if (pos) {
                newRegion = new CuboidRegion(x, y, z, r.getX2(), r.getY2(), r.getZ2());
            } else {
                newRegion = new CuboidRegion(r.getX1(), r.getY1(), r.getZ1(), x, y, z);
            }

            newRegion.drawSelection(p);
        }

        CuboidRegion.setRegion(
                    p,
                    newRegion);

        // cancel all further stuff
        e.setCancelled(true);
    }
}
