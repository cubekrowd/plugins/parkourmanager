package net.cubekrowd.parkourmanager.listeners;

import lombok.AllArgsConstructor;
import net.cubekrowd.parkourmanager.CancelRunZone;
import net.cubekrowd.parkourmanager.Course;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.Run;
import net.cubekrowd.parkourmanager.SimplePetsWrapper;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckpointAndZoneListener implements Listener {

    // private static Map<OfflineLocation, CourseAndCheckpoint> checkpointLocs = null;

    @AllArgsConstructor
    private static class CourseAndCheckpoint {
        private Course course;
        private Checkpoint checkpoint;
    }

    // Listening for PlayerInteractEvent doesn't work as well since it is only guaranteed to be fired once when the
    // player steps onto the pressure plate (although in practice it is sometimes called unpredictably while the player
    // is standing on the pressure plate).
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();

        // In practice this value is slightly too large, so we'll trim it to make it more consistent
        final double playerRadius = player.getBoundingBox().getWidthX() / 2 - 0.01;

        // Blocks the player was standing in
        Set<Location> previouslyOccupiedBlocks = getOccupiedBlocks(e.getFrom(), playerRadius);
        // Blocks the player is standing in
        Set<Location> currentlyOccupiedBlocks = getOccupiedBlocks(e.getTo(), playerRadius);

        // Blocks the player is walking out of
        Set<Location> oldBlocks = new HashSet<>(previouslyOccupiedBlocks);
        oldBlocks.removeAll(currentlyOccupiedBlocks);

        // Blocks the player is walking into
        Set<Location> newBlocks = new HashSet<>(currentlyOccupiedBlocks);
        newBlocks.removeAll(previouslyOccupiedBlocks);

        // @NOTE(cag).
        // The old method of detecting whether the player has entered a checkpoint
        // relied on a HashSet mapping individual coordinates to checkpoints.
        // Due to the new requirements, this would be extremely inefficient, because
        // the checkpoint locations may be arbitrary planes or cuboids, meaning
        // each block of a cubic region may have to be inserted into a HashMap.
        //
        // It is therefore more efficient to just check each checkpoint individually.
        // HashMaps, despite being O(1), have a very large constant factor in the
        // lookup time anyway.

        // buildCheckpointLocationsIfNecessary();

        // Get the first checkpoints of all courses.
        List<CourseAndCheckpoint> firstCheckpoints = ParkourManagerPlugin
                .getInstance()
                .getCourses()
                .stream()
                .map(c -> {
                    var cps = c.getCheckpoints();
                    if (cps.isEmpty()) return null;
                    if (!c.getWorldName().equals(player.getWorld().getName())) return null;

                    return new CourseAndCheckpoint(c, cps.get(0));
                })
                .toList();

        // Detection for leaving the first checkpoint of any course.

        // Detect leaving the first checkpoint of any course.
        for (CourseAndCheckpoint c : firstCheckpoints) {
            if (c == null)
                continue;

            for (Location oldBlock : oldBlocks) {
                if (c.checkpoint.containsLocation(oldBlock)) {
                    onCheckpoint(player, c.course, c.checkpoint, 0);
                    return;
                }
            }
        }

        // Get the checkpoint of the current run.
        final Run run = Run.getOrNull(player);
        if (run == null) return; // for safety :) not really needed though

        final Course course = run.getCourse();

        final var checkpoints = course.getCheckpoints();

        // Detect entering zones.
        // @NOTE(cag) re-used code, would be good to refactor
        for (CancelRunZone zone : course.getZones()) {

            boolean inZone = false;
            for (Location block : newBlocks) {
                if (zone.getRegion().containsLocation(block)) {
                    inZone = true;

                    // Only actiate zone if player was not previously in the checkpoint
                    if (!zone.containsPlayer(player)) {
                        zone.addPlayer(player);
                        // cancel run

                        Run.cancel(player);

                        ComponentBuilder message = new ComponentBuilder("Your run was cancelled because you left the course area")
                                .color(ChatColor.RED);
                        player.spigot().sendMessage(message.create());

                        run.sendResumeTipMessage();
                        
                        return;
                    }
                    
                    break;
                }
            }

            if (!inZone && newBlocks.size() > 0) {
                zone.removePlayer(player);
            }
        }

        // Detect entering any other new checkpoint.
        for (int i = 1; i < checkpoints.size(); ++i) {
            Checkpoint c = checkpoints.get(i);

            boolean inCheckpoint = false;
            for (Location block : newBlocks) {
                if (c.containsLocation(block)) {
                    inCheckpoint = true;

                    // Only actiate checkpoint if player was not previously in the checkpoint
                    if (!c.containsPlayer(player)) {
                        onCheckpoint(player, course, c, i);
                        c.addPlayer(player);
                    }
                    
                    break;
                }
            }

            if (!inCheckpoint && newBlocks.size() > 0) {
                c.removePlayer(player);
            }
        }
    }

    private Set<Location> getOccupiedBlocks(Location location, double playerRadius) {
        Set<Location> result = new HashSet<>();

        result.add(location.clone().add(playerRadius, 0, playerRadius).getBlock().getLocation());
        result.add(location.clone().add(playerRadius, 0, -playerRadius).getBlock().getLocation());
        result.add(location.clone().add(-playerRadius, 0, playerRadius).getBlock().getLocation());
        result.add(location.clone().add(-playerRadius, 0, -playerRadius).getBlock().getLocation());

        return result;
    }

    private void onCheckpoint(Player player, Course course, Checkpoint checkpoint, int index) {
        if (SimplePetsWrapper.isPlayerRidingPet(player) || player.isFlying()) {
            return;
        }

        // Starting a course that only has a single checkpoint can cause issues
        if (course.getCheckpoints().size() < 2) {
            return;
        }

        Run runNullable = Run.getOrNull(player);
        if (runNullable == null) {
            if (index == 0) {
                // This won't start a course if a run is already active
                Run.getOrStart(player, course);
                checkpoint.applyEffects(player);
            }
        } else {
            if (index == 0 && runNullable.hasReachedCheckpoint()) {
                Run.cancel(player);
                runNullable = Run.getOrStart(player, course);
            }

            if (runNullable == null) return;

            // Only activate the checkpoint if it's in the current course
            if (runNullable.getCourse().getCheckpoints().contains(checkpoint)) {
                runNullable.onCheckpoint(checkpoint);
            }
        }
    }

    /*
    private void buildCheckpointLocationsIfNecessary() {
        if (checkpointLocs == null) {
            checkpointLocs = new HashMap<>();

            for (Course course : ParkourManagerPlugin.getInstance().getCourses()) {
                for (RestorableCheckpoint checkpoint : course.getCheckpoints()) {
                    var location = new OfflineLocation(course.getWorldName(), checkpoint.getX(), checkpoint.getY(), checkpoint.getZ());
                    CourseAndCheckpoint entry = new CourseAndCheckpoint(course, checkpoint);

                    checkpointLocs.put(location, entry);
                }
            }
        }
    }


    public static void invalidateCheckpointLocations() {
        checkpointLocs = null;
    }

    */
}
