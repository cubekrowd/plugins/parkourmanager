package net.cubekrowd.parkourmanager.listeners;

import net.cubekrowd.lobbygames.ClickableItemUseEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ItemClickListener implements Listener {

    public static final String RETRY_ID = "parkour-retry";
    public static final String LAST_CHECKPOINT_ID = "parkour-last-checkpoint";

    @EventHandler
    private void onItemClick(ClickableItemUseEvent e) {
        switch (e.getActionId()) {
            case RETRY_ID:
                e.getPlayer().performCommand("parkour retry");
                break;
            case LAST_CHECKPOINT_ID:
                e.getPlayer().performCommand("parkour last-checkpoint");
                break;
        }
    }
}
