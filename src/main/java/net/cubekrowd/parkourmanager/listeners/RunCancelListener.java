package net.cubekrowd.parkourmanager.listeners;

import net.cubekrowd.lobbygames.GameStartedEvent;
import net.cubekrowd.parkourmanager.Run;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class RunCancelListener implements Listener {

    // When returning to a checkpoint this is set to true to avoid cancelling the run
    private static boolean shouldIgnoreNextTeleport = false;

    @EventHandler
    private void onPlayerTeleport(PlayerTeleportEvent e) {
        // Prevents teleportation due to lag or duplicated teleportation events from cancelling runs
        if (e.getCause() != PlayerTeleportEvent.TeleportCause.PLUGIN) {
            return;
        }

        if (shouldIgnoreNextTeleport) {
            shouldIgnoreNextTeleport = false;
            return;
        }

        Run.cancel(e.getPlayer());
    }

    @EventHandler
    private void onPlayerToggleFlight(PlayerToggleFlightEvent e) {
        if (e.isFlying()) {
            Player player = e.getPlayer();

            Run run = Run.getOrNull(player);
            if (run != null) {
                Run.cancel(player);

                ComponentBuilder message = new ComponentBuilder("Your run was cancelled because you started flying")
                        .color(ChatColor.RED);
                player.spigot().sendMessage(message.create());

                run.sendResumeTipMessage();
            }
        }
    }

    @EventHandler
    private void onPlayerLeave(PlayerQuitEvent e) {
        Run.cancel(e.getPlayer());
    }

    @EventHandler
    private void onGameStarted(GameStartedEvent e) {
        if (!e.getMinigame().getName().equals("parkour")) {
            Player player = e.getPlayer();

            Run run = Run.getOrNull(player);
            if (run != null) {
                Run.cancel(player);

                ComponentBuilder message = new ComponentBuilder("Your run was canceled because you started a new game")
                        .color(ChatColor.RED);
                player.spigot().sendMessage(message.create());

                run.sendResumeTipMessage();
            }
        }
    }

    public static void ignoreNextTeleport() {
        shouldIgnoreNextTeleport = true;
    }
}
