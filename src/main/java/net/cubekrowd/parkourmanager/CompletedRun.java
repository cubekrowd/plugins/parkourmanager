package net.cubekrowd.parkourmanager;

import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.result.DeleteResult;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.bukkit.entity.Player;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Data
@NoArgsConstructor
public class CompletedRun {

    @BsonId
    private ObjectId runId = ObjectId.get();

    private String playerUuid;
    private String courseName;
    // Measured in milliseconds
    private long duration;
    private Date when;

    public CompletedRun(Player player, Course course, long duration, Date when) {
        this.duration = duration;
        this.when = when;

        playerUuid = player.getUniqueId().toString();
        courseName = course.getName();
    }

    public CompletableFuture<Void> save() {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("records", CompletedRun.class);
        return ListSubscriber.subscribeWithoutResult(collection.insertOne(this));
    }

    public static CompletableFuture<List<DeleteResult>> deleteById(ObjectId id) {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("records", CompletedRun.class);
        return ListSubscriber.subscribe(collection.deleteOne(Filters.eq("_id", id)));
    }

    public static CompletableFuture<List<CompletedRun>> fetchRuns(
            Course courseNullable,
            UUID playerUuidNullable,
            boolean allowDuplicatePlayers,
            boolean sortByRecency,
            boolean weekly,
            int pageNumber,
            int pageSize
    ) {
        assert courseNullable != null || playerUuidNullable != null;

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("records", CompletedRun.class);

        // Filter only by attributes that are actually provided
        Bson filter;
        if (courseNullable == null) {
            filter = Filters.eq("playerUuid", playerUuidNullable.toString());
        } else if (playerUuidNullable == null) {
            filter = Filters.eq("courseName", courseNullable.getName());
        } else {
            filter = Filters.and(
                    Filters.eq("courseName", courseNullable.getName()),
                    Filters.eq("playerUuid", playerUuidNullable.toString())
            );
        }

        if (weekly) {
            ZonedDateTime startOfWeek =  ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("UTC"))
                    .truncatedTo(ChronoUnit.DAYS)
                    .with(TemporalAdjusters.previousOrSame(DayOfWeek.SATURDAY));

            Date startOfWeekDate = Date.from(startOfWeek.toInstant());

            filter = Filters.and(
                    filter,
                    Filters.gte("when", startOfWeekDate)
            );
        }

        Bson sort = sortByRecency ? Sorts.descending("when") : Sorts.ascending("duration");

        if (allowDuplicatePlayers) {
            return ListSubscriber.subscribe(
                    collection.find()
                            .filter(filter)
                            .sort(sort)
                            .skip(pageNumber * pageSize)
                            .limit(pageSize)
            );
        } else {
            return ListSubscriber.subscribe(
                    collection.aggregate(List.of(
                            Aggregates.match(filter),
                            Aggregates.sort(sort),
                            Aggregates.group("$playerUuid", Accumulators.first("record", "$$ROOT")),
                            Aggregates.replaceRoot("$record"),
                            Aggregates.sort(sort),
                            Aggregates.skip(pageNumber * pageSize),
                            Aggregates.limit(pageSize)
                    ))
            );
        }
    }
}
