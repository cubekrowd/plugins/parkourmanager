package net.cubekrowd.parkourmanager;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;

import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.cubekrowd.parkourmanager.checkpointeffects.CheckpointEffect;
import net.cubekrowd.parkourmanager.checkpoints.Checkpoint;
import net.cubekrowd.parkourmanager.checkpoints.CuboidCheckpoint;
import net.cubekrowd.parkourmanager.commands.cancelzone.CancelZoneCommandGroup;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.reactivestreams.Publisher;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Course {

    @EqualsAndHashCode.Include
    private String name;

    private String worldName;
    private String serverId = ParkourManagerPlugin.getServerId();

    private List<Checkpoint> checkpoints = new ArrayList<>();
    private List<CancelRunZone> zones = new ArrayList<>();

    public Course(String name, String worldName) {
        this.name = name;
        this.worldName = worldName;
    }

    public CompletableFuture<Void> save() {
        Set<Course> courses = ParkourManagerPlugin.getInstance().getCourses();
        // Force any properties to be updated
        courses.remove(this);
        courses.add(this);

        // Rebuild cached map of blocks to checkpoints
        // CheckpointListener.invalidateCheckpointLocations();

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);
        return ListSubscriber.subscribeWithoutResult(collection.replaceOne(
                Filters.and(Filters.eq("name", name), Filters.eq("serverId", serverId)),
                this,
                new ReplaceOptions().upsert(true)
        ));
    }

    public CompletableFuture<Void> remove() {
        ParkourManagerPlugin.getInstance().getCourses().remove(this);

        // Rebuild cached map of blocks to checkpoints
        // CheckpointListener.invalidateCheckpointLocations();

        PartialRun.clearByCourse(this).whenComplete((nothing, error) -> {
            if (error != null) {
                ParkourManagerPlugin.getLog().log(Level.SEVERE, "Failed to clear partial runs for " + name + " during deletion");
            }
        });

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);
        return ListSubscriber.subscribeWithoutResult(collection.deleteOne(Filters.eq("name", name)));
    }

    @SneakyThrows({ InterruptedException.class, ExecutionException.class})
    public static Course copyFromServer(String originalName, String otherServerId, String newName, String newWorld) {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);

        List<Course> results = ListSubscriber.subscribeAndAwait(collection.find(
                Filters.and(Filters.eq("name", originalName), Filters.eq("serverId", otherServerId))
        ));

        if (results.size() == 0) {
            throw new NoSuchElementException("course " + originalName + " not found on server " + otherServerId);
        }

        Course course = results.get(0);
        course.setServerId(ParkourManagerPlugin.getServerId());
        course.setName(newName);
        course.setWorldName(newWorld);

        course.save().get();
        return course;
    }

    public static Course byName(String name) {
        return ParkourManagerPlugin.getInstance().getCourses().stream()
                .filter(course -> course.getName().equals(name))
                .findAny()
                .orElseThrow();
    }

    public static boolean exists(String name) {
        return ParkourManagerPlugin.getInstance().getCourses().stream()
                .anyMatch(course -> course.getName().equals(name));
    }


    /*
    Query:

    db.courses.updateMany(
    {},
    {
        "$set": {
            "checkpoints.$[a].checkpoint-type": "restorable"
        }
    },
    {
        "arrayFilters":
            [
                {
                    "a.checkpoint-type": {
                        $exists: false
                    }
                }
            ]
        }
    )
    */
    public static void migrateData() {
        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses");

        Bson query = new Document();

        Bson updates = Updates.set("checkpoints.$[a].checkpoint-type", "single-block");

        var arrayFilters = new UpdateOptions().arrayFilters(Arrays.asList(
                Filters.exists("a.checkpoint-type", false)
        ));

        Publisher<UpdateResult> pub = collection.updateMany(query, updates, arrayFilters);
        UpdateResult res = ResultSubscriber.subscribeAndAwait(pub);

        if (res.getModifiedCount() > 0) {
            ParkourManagerPlugin.getLog().log(Level.INFO, "Migrated " + res.getModifiedCount() + " courses to the new format.");
        }
    }

    public static Set<Course> loadFromDatabase() {
        migrateData();

        var collection = ParkourManagerPlugin.getInstance().getMongoCollection("courses", Course.class);

        List<Course> courses = ListSubscriber.subscribeAndAwait(collection.find().filter(
                Filters.eq("serverId", ParkourManagerPlugin.getServerId())
        ));

        return new HashSet<>(courses);

    }

    // Generate the message for /parkour checkpoints list
    public BaseComponent[] generateCheckpointsMessage() {
        ComponentBuilder message = new ComponentBuilder("Checkpoints for " + getName() + " (click to tp):").color(ChatColor.GREEN).bold(true);
        
        for (int i = 0; i < checkpoints.size(); i++) {
            Checkpoint checkpoint = checkpoints.get(i);

            message.append("\n" + (i + 1) + ". ")
                    .color(ChatColor.GRAY)
                    .event(new ClickEvent(
                            ClickEvent.Action.RUN_COMMAND,
                            "/parkour checkpoint teleport " + getName() + " " + (i + 1)
                    ))
                    .bold(false);

            message.append(checkpoint.toDisplayString()).color(ChatColor.AQUA);

            // If it is a cuboid checkpoint, add a button to preview it.
            if (checkpoint instanceof CuboidCheckpoint) {
                // Button to show this checkpoint
                message.append(" [").color(ChatColor.GRAY);
                message.append("Show in World").color(ChatColor.WHITE)
                    .event(new ClickEvent(
                        ClickEvent.Action.RUN_COMMAND,
                        "/parkour region showcheckpoint " + getName() + " " + (i + 1)
                ));;
                message.append("]").color(ChatColor.GRAY);
            }
            
            List<CheckpointEffect> effects = checkpoint.getEffects();

            // Restorable
            message.append("\n ┗ Restorable: " + (checkpoint.isRestorable() ? "True" : "False") + " [").color(ChatColor.GRAY).event((ClickEvent) null);
            message.append("Toggle")
                .color(ChatColor.WHITE)
                .event(
                    new ClickEvent(
                        ClickEvent.Action.RUN_COMMAND, 
                        "/parkour checkpoint setrestorable " + getName() + " " + (i + 1) + " "
                    )
                );
            message.append("]").color(ChatColor.GRAY);

            // Effects
            message.append("\n ┗ Effects: (" + effects.size() + ")").color(ChatColor.GRAY);
            message.event((ClickEvent) null);

            // Add effect button for this checkpoint
            message.append(" [").color(ChatColor.GRAY);
            message.append("Add Effect").color(ChatColor.WHITE)
                .event(new ClickEvent(
                    ClickEvent.Action.RUN_COMMAND,
                    "/parkour effect menu " + getName() + " " + (i + 1)
            ));;
            message.append("]").color(ChatColor.GRAY);
            message.event((ClickEvent) null);

            // Append effects
            for (int j = 0; j < effects.size(); ++j) {
                CheckpointEffect effect = effects.get(j);
                message.append("\n   (" + (j + 1) + ") ").color(ChatColor.GRAY);
                
                message.append(effect.toDisplayString())
                    .color(ChatColor.GOLD);

                // [Remove] effect button
                message.append("\n     ┗ [").color(ChatColor.GRAY);
                message.append("Delete Effect").color(ChatColor.WHITE)
                    .event(new ClickEvent(
                        ClickEvent.Action.SUGGEST_COMMAND,
                        "/parkour effect remove " + getName() + " " + (i + 1) + " " + (j + 1) + " true"
                ));;
                message.append("]").color(ChatColor.GRAY);
            }
        }

        return message.create();
    } 

    // Generate the message for /parkour cancelzone list
    public BaseComponent[] generateZonesMessage() {
        ComponentBuilder message = new ComponentBuilder("Cancel zones for " + getName() + " (click to tp):").color(ChatColor.AQUA);
        
        for (int i = 0; i < zones.size(); i++) {
            CancelRunZone zone = zones.get(i);

            message.append("\n" + (i + 1) + ". ")
                    .color(ChatColor.GRAY)
                    .event(new ClickEvent(
                            ClickEvent.Action.RUN_COMMAND,
                            "/parkour cancelzone teleport " + getName() + " " + (i + 1)
                    ));

            message.append(zone.toDisplayString()).color(ChatColor.AQUA);

            // Add effect button for this checkpoint
            message.append(" [").color(ChatColor.GRAY);
            message.append("Show in World").color(ChatColor.WHITE)
                .event(new ClickEvent(
                    ClickEvent.Action.RUN_COMMAND,
                    "/parkour cancelzone show " + getName() + " " + (i + 1)
            ));;
            message.append("]").color(ChatColor.GRAY);
        }

        return message.create();
    } 
}
