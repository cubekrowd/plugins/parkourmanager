package net.cubekrowd.parkourmanager;

import lombok.Getter;
import lombok.SneakyThrows;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

// Asynchronously reads the result of a database operation into a list
public class ListSubscriber<T> implements Subscriber<T> {

    // Not modified after it is made public, so synchronous collection is fine
    private final List<T> documents = new ArrayList<>();

    @Getter
    private final CompletableFuture<List<T>> future = new CompletableFuture<>();

    @Override
    public void onSubscribe(Subscription s) {
        s.request(Long.MAX_VALUE);
    }

    @Override
    public void onNext(T t) {
        documents.add(t);
    }

    @Override
    public void onError(Throwable t) {
        future.completeExceptionally(t);
    }

    @Override
    public void onComplete() {
        future.complete(documents);
    }

    @SneakyThrows
    public List<T> await() {
        return future.get(45, TimeUnit.SECONDS);
    }

    public static <T> List<T> subscribeAndAwait(Publisher<T> publisher) {
        ListSubscriber<T> subscriber = new ListSubscriber<>();
        publisher.subscribe(subscriber);
        return subscriber.await();
    }

    public static <T> CompletableFuture<List<T>> subscribe(Publisher<T> publisher) {
        ListSubscriber<T> subscriber = new ListSubscriber<>();
        publisher.subscribe(subscriber);
        return subscriber.getFuture();
    }

    public static <T> CompletableFuture<Void> subscribeWithoutResult(Publisher<T> publisher) {
        return subscribe(publisher).thenApply(result -> null);
    }
}
