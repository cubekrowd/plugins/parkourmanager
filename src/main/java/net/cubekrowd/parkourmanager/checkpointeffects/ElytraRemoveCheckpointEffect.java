package net.cubekrowd.parkourmanager.checkpointeffects;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.cubekrowd.parkourmanager.Run;
import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bukkit.entity.Player;

@Data
@NoArgsConstructor
@BsonDiscriminator(key = "effect-type", value = "elytra-removal")
public class ElytraRemoveCheckpointEffect extends CheckpointEffect {
    @Override
    public void apply(Player player) {
        Run run = Run.getOrNull(player);
        if (run == null) {
            return;
        }

        run.removeFireworks();
        run.removeElytra();
    }

    @Override
    public String toDisplayString() {
        return "Remove Elytra";
    }
}
