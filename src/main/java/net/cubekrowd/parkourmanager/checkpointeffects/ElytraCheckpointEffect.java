package net.cubekrowd.parkourmanager.checkpointeffects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.cubekrowd.parkourmanager.ParkourManagerPlugin;
import net.cubekrowd.parkourmanager.Run;
import net.cubekrowd.parkourmanager.util.ItemTagger;
import net.md_5.bungee.api.ChatColor;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.persistence.PersistentDataType;

import static java.lang.Math.min;

@Data
@NoArgsConstructor
@AllArgsConstructor
@BsonDiscriminator(key = "effect-type", value = "elytra")
public class ElytraCheckpointEffect extends CheckpointEffect {

    // -1 for unbreakable.
    @BsonProperty
    private int elytraDurability = -1;

    // -1 for infinite fireworks.
    @BsonProperty
    private int numFireworks = -1;

    static final short ELYTRA_MAX_DURABILITY = 432;

    void giveElytra(Player player, Run run) {
        ItemStack elytra;

        elytra = new ItemStack(Material.ELYTRA);

        if (elytraDurability < 0) {
            elytra = new ItemStack(Material.ELYTRA);
            
            var data = elytra.getItemMeta();
            data.setUnbreakable(true);
            elytra.setItemMeta(data);

        } else {

            int damage;

            if (elytraDurability > ELYTRA_MAX_DURABILITY) {
                damage = 0;
            } else {
                damage = ELYTRA_MAX_DURABILITY - elytraDurability;
            }

            Damageable data = (Damageable) elytra.getItemMeta();

            if (elytraDurability < 0) {
                data.setUnbreakable(true);
            } else {
                data.setDamage(damage);
            }

            elytra.setItemMeta(data);

        }

        // tag the elytra given to the player so we can keep track of it
        ItemTagger.tagItemsStack(elytra, "parkour-elytra");

        // give curse of binding so the player cannot remove :)
        elytra.addEnchantment(Enchantment.BINDING_CURSE, 1);

        // If the player has elytra right now, don't set
        if (!run.getElytraState().shouldHaveElytra) {
            run.getElytraState().previousChestplate = player.getInventory().getChestplate();
        }

        player.getInventory().setChestplate(elytra);
        run.getElytraState().shouldHaveElytra = true;
    }

    public static void giveFireworks(Player player, int num) {
        Run run = Run.getOrNull(player);

        ItemStack fireworks;

        if (num < 0) {
            fireworks = new ItemStack(Material.FIREWORK_ROCKET, 1);
            // tag it with the unlimited firework tag
            ItemTagger.tagItemsStack(fireworks, "parkour-fireworks-unlimited");
            // set a name :P

            var data = fireworks.getItemMeta();
            data.setDisplayName(ChatColor.RESET + "" + ChatColor.AQUA + "Unlimited Firework Rocket");
            fireworks.setItemMeta(data);
        } else {
            fireworks = new ItemStack(Material.FIREWORK_ROCKET, min(64, num));
        }

        FireworkMeta data = (FireworkMeta) fireworks.getItemMeta();
        data.setPower(1);

        fireworks.setItemMeta(data);

        // tag all fireworks given to the player by the plugin, so we can keep track of
        // them
        ItemTagger.tagItemsStack(fireworks, "parkour-fireworks");

        player.getInventory().addItem(fireworks);

        run.getElytraState().fireworksStack = fireworks;
    }

    void giveFireworks(Player player) {
        Run run = Run.getOrNull(player);
        // clear old fireworks
        run.removeFireworks();
        giveFireworks(player, numFireworks);
    }

    @Override
    public void apply(Player player) {
        Run run = Run.getOrNull(player);
        if (run == null) {
            return;
        }

        // Give items
        giveElytra(player, run);
        giveFireworks(player);
    }

    @Override
    public String toDisplayString() {
        String durabilityStr = elytraDurability < 0 ? "∞"
                : Integer.toString(min(elytraDurability, ELYTRA_MAX_DURABILITY));

        String numFireworksStr = numFireworks < 0 ? "∞" : Integer.toString(min(numFireworks, 64));

        return "Give Elytra [Durability: " + durabilityStr + ", Fireworks: " + numFireworksStr + "]";
    }
}
