package net.cubekrowd.parkourmanager.checkpointeffects;

import lombok.Data;
import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bukkit.entity.Player;

// Checkpoint effects are modifiers given to the player once they reach a checkpoint or respawn there.
// This could include being given elytra, potion effects, or velocity packets.

@Data
@BsonDiscriminator(key = "effect-type")
public abstract class CheckpointEffect {
    @BsonProperty("effect-type")
    protected String effectType;

    // Applies the effect to the specified player.
    public abstract void apply(Player player);

    public abstract String toDisplayString();
}
