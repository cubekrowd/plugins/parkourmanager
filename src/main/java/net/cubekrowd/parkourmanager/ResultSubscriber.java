package net.cubekrowd.parkourmanager;

import lombok.Getter;
import lombok.SneakyThrows;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

// Modified version of ListSubscriber
public class ResultSubscriber<T> implements Subscriber<T> {

    private T val;

    @Getter
    private final CompletableFuture<T> future = new CompletableFuture<>();

    @Override
    public void onSubscribe(Subscription s) {
        s.request(Long.MAX_VALUE);
    }

    @Override
    public void onNext(T t) {
        val = t;
    }

    @Override
    public void onError(Throwable t) {
        future.completeExceptionally(t);
    }

    @Override
    public void onComplete() {
        future.complete(val);
    }

    @SneakyThrows
    public T await() {
        return future.get(45, TimeUnit.SECONDS);
    }

    public static <T> T subscribeAndAwait(Publisher<T> publisher) {
        ResultSubscriber<T> subscriber = new ResultSubscriber<>();
        publisher.subscribe(subscriber);
        return subscriber.await();
    }

    public static <T> CompletableFuture<T> subscribe(Publisher<T> publisher) {
        ResultSubscriber<T> subscriber = new ResultSubscriber<>();
        publisher.subscribe(subscriber);
        return subscriber.getFuture();
    }

    public static <T> CompletableFuture<Void> subscribeWithoutResult(Publisher<T> publisher) {
        return subscribe(publisher).thenApply(result -> null);
    }
}
